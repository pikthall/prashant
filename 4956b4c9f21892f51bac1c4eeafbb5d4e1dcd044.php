<script src="https://checkout.stripe.com/checkout.js"></script>
<style type="text/css">
.pay_with{ display:inline-block; padding:8px 15px; border-radius: 4px; background:skyblue; color:#fff; margin:20px 0;}
.pay_with:hover, .pay_with:focus{ color:#fff;}
.card_sec_form{ display:inline-block; max-width: 800px; width:100%; padding:20px 0;}
.card_sec_form .inn1{ width:100%; height: 50px; border: none;background: none; border-bottom: 1px solid #ddd; outline: none;text-align: left;}
.card_sec_form label{ font-weight: normal; margin-bottom: 0px; margin-top: 5px; text-align: left; width:100%;}
.card_sec_form .card_submit{ color: orange; background:none; outline:none; border:none; margin-top: 60px; font-size: 60px;}
</style>

<div class="col-lg-3">&nbsp;</div>
<div class="col-lg-6">

    <div class="modesPayment">
        <div class="row">
            <div class="col-md-7 lblpayment"><?php echo e($package->name); ?> package</div>
            <div class="col-md-5 priceSection">£<span id="package-price"><?php echo e($package_price); ?></span></div>
            <div id="choiced-service"></div>
            <div class="clearfix"></div>
            <div class="borderBtm"></div>
            <div class="col-md-7 totallabelCol">TOTAL</div>
            <div class="col-md-5 totalCol">
                <div class="priceSection">£<span id="total-amount"><?php echo e($package_price); ?></span></div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <input type="text" class="text-center discountCodeLabel">
            </div>
            <div class="col-md-6"><button class="btn discountCode" type="button">Add Discount Code</button></div>
            <span id="discount-error" class="help-block error-help-block" style="font-size: 14px"></span>
            <div class="clearfix"></div>

            <div class="col-md-7 discountSection totallabelCol" hidden>With Discount (<span id="percent"></span>%)</div>
            <div class="col-md-5 totalCol">
                <div class="discountSection priceSection" hidden>£<span id="with-discount"></span></div>
            </div>



        </div>
    </div>

    <div class="methodPayment">


        <form action="<?php echo e(url('Stripe/'.$package->name)); ?>" method="POST" id='Stripe_form'>
            <?php echo csrf_field(); ?>

            <?php echo Form::hidden('type',$package->name); ?>

        <?php echo Form::hidden('pay',$package_price); ?>

        <?php echo Form::hidden('discount',''); ?>

        <?php echo Form::hidden('custom',''); ?>

        <a href="javascript:;" class="pay_with" id="click1">Pay with Card</a>

                <?php if($package->name != \App\ConfigPackegType::TYPE_PAYG ): ?>
        <p>Select payment method</p>

        <?php echo Form::open(array('route' => 'getCheckout','id'=>'paypal-form')); ?>

        <?php echo Form::hidden('type',$package->name); ?>

        <?php echo Form::hidden('pay',$package->price); ?>

        <?php echo Form::hidden('discount',''); ?>

        <?php echo Form::hidden('custom',''); ?>

        <?php echo Form::close(); ?>


        <div class="payment-icons paypal-icon"><img src="<?php echo e(asset('images/paypal-active-icon.jpg')); ?>" alt="" width="90" height="50" class="am-express"></div>
        <div class="payment-icons " data-key="<?php echo e(config('services.stripe.public')); ?>"><img src="<?php echo e(asset('images/am-express-icon.jpg')); ?>" alt="" width="90" height="50" class="am-express" data-toggle="tooltip" data-placement="top" title="Coming Soon, please use PayPal or Contact Us"></div>
        <div class="payment-icons " data-key="<?php echo e(config('services.stripe.public')); ?>"><img src="<?php echo e(asset('images/visa-icon.png')); ?>" alt="" width="90" height="50" class="visa" data-toggle="tooltip" data-placement="top" title="Coming Soon, please use PayPal or Contact Us"></div>
        <div class="payment-icons " data-key="<?php echo e(config('services.stripe.public')); ?>"><img src="<?php echo e(asset('images/mastercard-icon.jpg')); ?>" alt="" width="90" height="50" class="mastercard" data-toggle="tooltip" data-placement="top" title="Coming Soon, please use PayPal or Contact Us"></div> 
            <div class="clearfix"></div>
                <div class="card_sec_form" id="show_sec1" style="display: none">
                <div class="row">
                <div class="col-md-10">
                <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                    <input type="text" class="inn1">
                    <label>Name on Card</label>
                </div>
                </div>
                <div class="col-md-6">
                 <div class="form-group">
                    <input type="text" class="inn1" size="20" data-stripe="number">
                    <label>Card Number</label>
                </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                    <input type="text" class="inn1" size="6" data-stripe="address_zip">
                    <label>ZIP/Postal Code</label>
                </div>
                </div>
                <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="inn1" size="2" data-stripe="exp_month>
                    <label>Expiry Date</label>
                </div>
                </div>
                <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="inn1">
                    <label>Secyrity Code</label>
                </div>
                </div>
                </div>
                </div>
                <div class="col-md-2"><button type="submit" class="card_submit" class="submit" value="Submit Payment"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></button></div>
                </div>
                </div>
                </div>

        </form>

        <!--        
                
                <button id="customButton">Purchase</button>
        
                <script>
        var handler = StripeCheckout.configure({
            key: 'pk_test_8HrgurhXPuPZkPn7J8abGUra',
            image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
            locale: 'auto',
            token: function (token) {
                // You can access the token ID with `token.id`.
                // Get the token ID to your server-side code for use.
            }
        });
        
        document.getElementById('customButton').addEventListener('click', function (e) {
            // Open Checkout with further options:
            handler.open({
                name: 'Demo Site',
                description: '2 widgets',
                currency: 'cad',
                amount: 2000
            });
            e.preventDefault();
        });
        
        // Close Checkout on page navigation:
        window.addEventListener('popstate', function () {
            handler.close();
        });
                </script>-->
   
         <!--<div class="payment-icons adyen-icon"><img src="<?php echo e(asset('images/adyen-icon.jpg')); ?>" alt="" width="90" height="50" class="adyen"></div>-->
        <div class="test"></div>
        <?php else: ?>
        <div class="modesPayment">
            <div class="row">
                <div class="col-md-3">
                    <input type="hidden" class="text-center discountCodeLabel">
                </div>
                <div class="col-md-6"><button class="btn proceed adyen-icon"  type="button">Click to proceed</button></div>

            </div>
        </div>
        <?php endif; ?>

        <div id="adyen-form"></div>

    </div>   
</div>

<script>
    jQuery(document).ready(function () {
        $('*[data-toggle=tooltip]').tooltip()


    });
</script>

<!--<div class="col-lg-3">
    <div class="btnSectionInner">
        <div class="nextBtnContainer custom">&nbsp;</div>  
    </div>
</div>-->
<!--<div class="clearfix"></div>-->
<!-- OPTION 5 ----------------------------------------------->
<!--<div class="col-lg-2">&nbsp;</div>-->
<!--<div class="col-lg-7 formCont customPayment" >
    <div class="row">
        <div class="container-fluid customContainer">
            <?php echo Form::open(array('route' => 'purchase-payment-info', 'id'=>'payment-card','class' => 'form')); ?>

            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group">
                        <?php echo Form::text('name_on_card', null,  array('required', 'class'=>'form-control inputfieldCustom')); ?>

                    </div>
                    <?php echo Form::label('name_on_card','Name on card',array('class'=>'control-label col-lg-12 custom')); ?>

                </div>
                <div class="col-lg-6">
                    <div class="input-group">
                        <?php echo Form::text('card_number', null,  array('required', 'class'=>'form-control inputfieldCustom')); ?>

                    </div>
                    <?php echo Form::label('card_number','Card number',array('class'=>'control-label col-lg-12 custom')); ?>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group">
                        <input type="text" class="form-control inputfieldCustom"/>
                    </div>
                    <label class="control-label col-lg-12 custom">ZIP/Postal code</label>
                </div>
                <div class="col-lg-3">
                    <div class="input-group">
                        <?php echo Form::text('expiry_date', null,  array('required', 'class'=>'form-control inputfieldCustom')); ?>

                    </div>
                    <?php echo Form::label('expiry_date','Expiry date',array('class'=>'control-label col-lg-12 custom')); ?>

                </div>
                <div class="col-lg-3">
                    <div class="input-group">
                        <?php echo Form::text('sequrity_code', null,  array('required', 'class'=>'form-control inputfieldCustom')); ?>

                    </div>
                    <?php echo Form::label('sequrity_code','Security code',array('class'=>'control-label col-lg-12 custom')); ?>

                </div>
                <div class="form-group">
                    <?php echo Form::submit('Save!', array('class'=>'btn btn-primary')); ?>

                </div>
            </div>
            <?php echo Form::close(); ?>

            <?php echo JsValidator::formRequest('App\Http\Requests\PaymentRequest')->selector('#payment-card'); ?>

        </div>
    </div>
</div>-->
<!--<div class="col-lg-3 btnSection">
    <div class="btnSectionInner">
        <div class="nextBtnContainer customPayment">&nbsp;</div>
    </div>
</div>-->
<!-- /OPTION 5 ------------------------------------------------>
<script type="text/javascript">
$( "#click1" ).click(function() {

  $( "#show_sec1" ).slideToggle( "slow", function() {
    // Animation complete.
  });
});</script>